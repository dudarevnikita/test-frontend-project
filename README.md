# Front-end test project

## Quick start


Ensure you have a reasonably fresh Node.js on your machine.

1. Enter `project/` directory
2. Execute `npm i`
3. Execute `gulp build` and check `dist/` directory

#### Author

**Nikita Dudarev**
