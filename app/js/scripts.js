jQuery(document).ready(function(){

    jQuery('li a, a.search').click(function(e){
        e.preventDefault();
    });

});

jQuery('.slider-content').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    focusOnSelect: false,
    swipeToSlide: true,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 1000,
    fade: true,
    prevArrow: jQuery('.prev'),
    nextArrow: jQuery('.next'),
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]

});